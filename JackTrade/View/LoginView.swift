//
//  ContentView.swift
//  JackTrade
//
//  Created by Vikku Ponnaganti on 2/16/21.
//

import SwiftUI

struct LoginView: View {
    
    @State var Username = ""
    @State var Password = ""
    
    var body: some View {
        VStack(spacing: 15) {
            Spacer()
            Text("Jack Trades")
                .font(.system(size: 52, weight: . semibold))
                .foregroundColor(.black)
            //Username
            HStack {
                Image(systemName: "person")
                    .foregroundColor(.black)
                TextField("Username", text: $Username)
                    .foregroundColor(.black)
            }
            .padding(.all, 20)
            .background(Color.gray)
            .cornerRadius(8)
            .padding(.horizontal, 20)
            //Password
            HStack {
                Image(systemName: "lock")
                    .foregroundColor(.black)
                SecureField("Password", text: $Password)
                    .foregroundColor(.black)
            }
            .padding(.all, 20)
            .background(Color.gray)
            .cornerRadius(8)
            .padding(.horizontal, 20)
            
            Button(action:  {
                print("\(self.Username) and \(self.Password)")
            }) {
                Text("Login")
                    .foregroundColor(.white)
                    .font(.system(size: 18, weight: .medium))
            }
            .frame(maxWidth: .infinity)
            .padding(.vertical, 10)
            .background(Color.red.opacity(0.8))
            .cornerRadius(8)
            .padding(.horizontal, 10)
            
            Spacer()
        }.background(
            Image("map")
                .resizable()
                .aspectRatio(contentMode: .fill)
        ).edgesIgnoringSafeArea(.all)
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            LoginView().previewDevice("iPod Touch")
            //LoginView().previewDevice("iPhone X")
            //LoginView().previewDevice("iPhone 11 Pro Max")
        }
    }
}

