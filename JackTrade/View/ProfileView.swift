//
//  ProfileView.swift
//  JackTrade
//
//  Created by Vikku Ponnaganti on 2/16/21.
//

import Foundation
import SwiftUI

struct CircleImage: View {
    var body: some View {
        Image("vikkuImg")
            .resizable()
            .frame(width: 200, height: 200)
            .aspectRatio(contentMode: .fit)
            .clipShape(Circle())
            .overlay(Circle().stroke(Color.white, lineWidth: 2))
            .shadow(radius: 10)
    }
}

struct ProfileView: View {
    var body: some View {
        VStack(){
            
            CircleImage().offset(y: -130).padding(.bottom, -130)
            
            VStack(alignment: .leading){
                Text("Vikku Ponnaganti")
                    .font(.title)
                HStack() {
                    Text("Computer Expert")
                        .font(.subheadline)
                    Spacer()
                    Text("Computer Science")
                }
            }.padding()
        }.edgesIgnoringSafeArea(.top)
    }
}

