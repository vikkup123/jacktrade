//
//  Dashboard.swift
//  JackTrade
//
//  Created by Vikku Ponnaganti on 2/16/21.
//

import SwiftUI

struct DashboardView: View {
    @State var selectedView = 1

    var body: some View {
        TabView(selection: $selectedView) {
            SearchView()
            .tabItem {
                Label("Search", systemImage: "1.circle")
            }
            .tag(1)
            MessagesView()
            .tabItem {
                Label("Messages", systemImage: "2.circle")
            }
            .tag(2)
            ProfileView()
            .tabItem {
                Label("Profile", systemImage: "3.circle")
            }
            .tag(3)
        }
    }
}

struct DashboardView_Previews: PreviewProvider {
    static var previews: some View {
        DashboardView()
    }
}



