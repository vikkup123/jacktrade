//
//  SearchView.swift
//  JackTrade
//
//  Created by Vikku Ponnaganti on 2/16/21.
//

import Foundation
import SwiftUI

struct SearchView: View {
    @State private var search: String = ""
    
    var body: some View {
        VStack {
            HStack {
                Image(systemName:
                        "magnifyingglass").foregroundColor(
                            .secondary).padding(.leading,10)
                TextField("Search for a provider", text:
                            $search)
                    .disableAutocorrection(true)
                    .padding(.trailing,10)
            }.padding()
            .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.gray, lineWidth: 1))
            
            Button("Search") {
                print("Search for:  \(search)")
                hideKeyboard()
            }
        }.padding(10)
    }
}

struct SearchView_Previews: PreviewProvider {
    static var previews: some View {
        SearchView()
    }
}

#if canImport(UIKit)
extension View {
    func hideKeyboard() {
        UIApplication.shared.sendAction(#selector(UIResponder
                                            .resignFirstResponder), to:nil, from: nil, for: nil)
    }
}
#endif

