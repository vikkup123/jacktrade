//
//  JackTradeApp.swift
//  JackTrade
//
//  Created by Vikku Ponnaganti on 2/16/21.
//

import SwiftUI

@main
struct JackTradeApp: App {
    var body: some Scene {
        WindowGroup {
            LoginView()
        }
    }
}
